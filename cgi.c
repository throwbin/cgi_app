#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define REQ_HEADER_SIZE 5
#define FILE_NAME "data.json"

char* fetch_json(char* filename);
void update_json(char* filename);
void print_GET(void);
void print_POST(void);

int fsize(FILE *fp);
char* read_stdin(void);
char* date_time_now(void);
void insert_datetime_field(FILE *fp, char* filename);
FILE* create_new_file(FILE *fp);

int main() {
    char header[REQ_HEADER_SIZE];

    strcpy(header, getenv("REQUEST_METHOD"));

    if(!strcmp(header, "GET")){
        print_GET();
    }
    else{
        print_POST();
    }

    return EXIT_SUCCESS;
}

char* fetch_json(char* filename){
    FILE * fp;
    int size;

    fp = fopen(filename, "r");
    if(!fp){
        perror("Error opening file\r\n");
    }

    // File length
    size = fsize(fp) + 1;

    // Associate memory on heap
    char* buffer = malloc((size_t) size);

    int i = 0;
    while(!feof(fp)){
        buffer[i] = (char)fgetc(fp);
        i++;
    }
    buffer[i - 1] = '\0';

    fclose(fp);

    return buffer;
}


void update_json(char* filename){
    FILE *fp;
    char* buffer;

    // Dynamic buffer using realloc()
    buffer = read_stdin();

    // Open file for read and overwrite
    fp = fopen(filename,"r+");

    int size = fsize(fp);

    // Does file exist or is it empty?
    if(!fp){
        // Close because created above
        fclose(fp);

        // New file pointer created
        fp = create_new_file(fp);
        fprintf(fp, "%s", buffer);
    }
    else if(!size){
        fprintf(fp, "%s", buffer);
    }
    else{
        fp = fopen(filename, "r+");

        size = fsize(fp);

        // Seek till the end (minus one) and append the new string
        fseek( fp, (size - 1), SEEK_SET);
        *(buffer) = ',';
        fprintf(fp, "%s", buffer);
    }

    fclose(fp);
    insert_datetime_field(fp, filename);
}

FILE* create_new_file(FILE *fp){
    fp = fopen("file.txt","w+");
    return fp;
}

char* date_time_now(void){
    char* str = malloc(20);
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    sprintf(str, "%d-%d-%d %d:%d:%d", tm.tm_year + 1900,
            tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

    return str;
}

void insert_datetime_field(FILE *fp, char* filename){
    // Make sure file is opened for read and overwrite
    fp = fopen(filename,"r+");

    // Determine file size
    int size = fsize(fp);

    // Use fseek() to overwrite part of the file
    fseek( fp, (size - 2), SEEK_SET);
    char* date_time = date_time_now();
    fprintf(fp, ",\"datetime\":\"%s\"}]", date_time);

    free(date_time);
    fclose(fp);
}

void print_GET(void){
    char* response;
    printf ("Content-Type: application/json\n");
    printf ("\n");

    response = fetch_json(FILE_NAME);
    printf("%s", response);
    free(response);
}

void print_POST(void){
    printf ("Content-Type: text/html\n");
    printf ("\n");

    update_json(FILE_NAME);

    printf("File successfully updated!\n");
}


char* read_stdin (void) {
    size_t cap = 4096, /* Initial capacity for the char buffer */
            len =    0; /* Current offset of the buffer */
    char *buffer = malloc(cap * sizeof (char));
    int c;

    /* Read char by char, breaking if we reach EOF or a newline */
    while ((c = fgetc(stdin)) != '\n' && !feof(stdin))
    {
        buffer[len] = (char) c;

        /* When cap == len, we need to resize the buffer
         * so that we don't overwrite any bytes
         */
        if (++len == cap)
            /* Make the output buffer twice its current size */
            buffer = realloc(buffer, (cap *= 2) * sizeof (char));
    }

    /* Trim off any unused bytes from the buffer */
    buffer = realloc(buffer, (len + 1) * sizeof (char));

    /* Pad the last byte so we don't overread the buffer in the future */
    buffer[len] = '\0';

    return buffer;
}

int fsize(FILE *fp){
    int prev=ftell(fp);
    fseek(fp, 0L, SEEK_END);
    int sz=ftell(fp);
    fseek(fp,prev, SEEK_SET);
    return sz;
}
